import http from 'http'


export class Route {

    // fonction pour deamnder les affiches des donnes
    static get(req:http.IncomingMessage,res:http.ServerResponse){
        let url:string|undefined=req.url;
        let method:string|undefined=req.method;

        if(url==='/' && method==='GET'){
            res.statusCode=200
            res.end(`<h2>Accueil </h2>`)
        }

        else if(url==='/about' && method==='GET'){
            res.end(`<h2>about</h2>`)
        }

        else if(url==='/infos' && method==='GET'){
            res.end(`<h2>infos</h2>`)
        }
        else {
            res.end(`<h2>vide</h2>`)
        }

    }
    
    // fonction pour poster et verifier  les contenus
    static post(req:http.IncomingMessage,res:http.ServerResponse){

        let url:string|undefined=req.url;
        let method:string|undefined=req.method;
        let body:any='';

        if(url==='/user' && method==='POST'){
            req.on('data',(chunk)=>{body+=chunk})
               .on('end',()=>{
                if(JSON.parse(body).name==="enock" && JSON.parse(body).email==="enock@gmail.com" ){
                    res.end(JSON.stringify(JSON.parse(body)))
                }
                else {
                    res.end(`invalid credentials`)
                }
                
            })
        }
    }
}