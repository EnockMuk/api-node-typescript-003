import http, { IncomingMessage,  Server, ServerResponse } from 'http';
import { Route} from './src/route';
// nom de l'hebergeur
const hostname:string ='127.0.0.1';
// le port sur lequel on va tourner le serveur
const port:number=2000;

// cree le serveur
const server:Server=http.createServer((req:IncomingMessage,res:ServerResponse)=>{
    res.statusCode=200;
    res.setHeader('content-type','text/html ')
    
    Route.get(req,res)
    Route.post(req,res)

   // res.end(`<h2>Bienvenu sur le serveur de Enock </h2>`)
    
})


//ecouter le serveur sur le port 
server.listen(port,hostname,()=>{console.log(`this server is runing on ${hostname} : ${port}`)})